package com.loquake.web.server.dao;

import java.util.Comparator;

import com.loquake.web.client.model.Earthquake;

public class EQDateComparator implements Comparator<Earthquake> {
  public int compare(Earthquake q1, Earthquake q2) {
    return q2.getDate().compareTo(q1.getDate());
  }
}
