package com.loquake.web.server.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EqDateParser {
  public Date parse(final String ds) throws ParseException {
    final DateFormat df = new SimpleDateFormat("EEEE, MMMM dd, yyyy HH:mm:ss z");
    return df.parse(ds);
  }
}
