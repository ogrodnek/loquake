package com.loquake.web.client.model;

import java.io.Serializable;
import java.util.Date;

public class Earthquake implements Serializable {
  
  private String src;
  private String eqid;
  private String version;
  private Date date;
  private float lat;
  private float lon;
  private float magnitude;
  private float depth;
  private int nst;
  private String region;
  
  public String getSrc() {
    return src;
  }
  public void setSrc(String src) {
    this.src = src;
  }
  public String getEqid() {
    return eqid;
  }
  public void setEqid(String eqid) {
    this.eqid = eqid;
  }
  public String getVersion() {
    return version;
  }
  public void setVersion(String version) {
    this.version = version;
  }
  public Date getDate() {
    return date;
  }
  public void setDate(Date date) {
    this.date = date;
  }
  public float getLat() {
    return lat;
  }
  public void setLat(float lat) {
    this.lat = lat;
  }
  public float getLon() {
    return lon;
  }
  public void setLon(float lon) {
    this.lon = lon;
  }
  public float getMagnitude() {
    return magnitude;
  }
  public void setMagnitude(float magnitude) {
    this.magnitude = magnitude;
  }
  public float getDepth() {
    return depth;
  }
  public void setDepth(float depth) {
    this.depth = depth;
  }
  public int getNst() {
    return nst;
  }
  public void setNst(int nst) {
    this.nst = nst;
  }
  public String getRegion() {
    return region;
  }
  public void setRegion(String region) {
    this.region = region;
  }

}
