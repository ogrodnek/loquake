package com.loquake.web.server.dao;

import java.util.List;

import com.loquake.web.client.model.Earthquake;

public interface EarthquakeDAO {
  List<Earthquake> getLatestForLocation(float lat, float lon, int num);
}
