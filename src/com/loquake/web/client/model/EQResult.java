package com.loquake.web.client.model;

import java.io.Serializable;

public class EQResult implements Serializable {
  private Earthquake quake;
  private int distance;
  private String when;
  
  public Earthquake getQuake() {
    return quake;
  }
  public void setQuake(Earthquake quake) {
    this.quake = quake;
  }
  public int getDistance() {
    return distance;
  }
  public void setDistance(int d) {
    this.distance = d;
  }
  public String getWhen() {
    return when;
  }
  public void setWhen(String when) {
    this.when = when;
  }
}
