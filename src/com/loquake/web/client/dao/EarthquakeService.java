package com.loquake.web.client.dao;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.loquake.web.client.model.EQResult;

@RemoteServiceRelativePath("eq_srv")
public interface EarthquakeService extends RemoteService {
  List<EQResult> getLatestForLocation(float lat, float lon, int num);
}
