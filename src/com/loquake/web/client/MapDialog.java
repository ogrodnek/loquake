package com.loquake.web.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.visualizations.MapVisualization;
import com.loquake.web.client.model.EQResult;
import com.loquake.web.client.model.Earthquake;

/**
 * Simple dialog to display a remote response.
 * 
 * @author larry
 */
final class MapDialog {
  private DialogBox dialogBox;
  private Button closeButton;
  private final VerticalPanel panel = new VerticalPanel();
  
  public MapDialog() {
    // Create the popup dialog box
    dialogBox = new DialogBox();
    dialogBox.setAnimationEnabled(true);
    dialogBox.setModal(false);
    closeButton = new Button("Close");
    
    closeButton.addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent e) {
        dialogBox.hide();
      }
    });
    
    final VerticalPanel main = new VerticalPanel();
    main.setSpacing(4);
    
    final VerticalPanel buttons = new VerticalPanel();
    buttons.add(closeButton);
    
    main.add(panel);
    main.add(buttons);
    main.setCellHorizontalAlignment(buttons, HasHorizontalAlignment.ALIGN_RIGHT);

    dialogBox.setWidget(main);
  }
  
  public void showMap(final EQResult r) {
    dialogBox.setText(r.getQuake().getRegion());
    final MapVisualization.Options mapOptions = MapVisualization.Options.create();
    mapOptions.setShowTip(true);
    mapOptions.setOption("zoomLevel", 6);
    final MapVisualization map = new MapVisualization(createMapTable(r), mapOptions, "300", "400");
    panel.clear();
    panel.add(map);
    dialogBox.center();
  }
  
  private DataTable createMapTable(final EQResult r) {
    final DataTable table = DataTable.create();
    table.addColumn(ColumnType.NUMBER, "lat");
    table.addColumn(ColumnType.NUMBER, "lon");
    table.addColumn(ColumnType.STRING, "name");
    
    table.addRows(1);
    
      final Earthquake e = r.getQuake();
      
      table.setValue(0, 0, e.getLat());
      table.setValue(0, 1, e.getLon());
      table.setValue(0, 2, e.getMagnitude() + " " + e.getRegion());
    
    return table;
  }
  
}