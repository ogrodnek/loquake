package com.loquake.web.server.dao;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.loquake.web.client.model.Earthquake;
import com.loquake.web.server.dao.distance.DistanceWithinPredicate;

public final class FileEarthquakeDAO implements EarthquakeDAO {
  private final List<Earthquake> earthquakes = new ArrayList<Earthquake>();
  
  public FileEarthquakeDAO(final String path) throws Exception {
    final String[][] vals = CSVParser.parse(new FileReader(path));
    final EqDateParser dp = new EqDateParser();
    
    for (int i=1; i< vals.length; i++) {
      final Earthquake q = new Earthquake();
      q.setSrc(vals[i][0]);
      q.setEqid(vals[i][1]);
      q.setVersion(vals[i][2]);
      q.setDate(dp.parse(vals[i][3]));
      q.setLat(Float.parseFloat(vals[i][4]));
      q.setLon(Float.parseFloat(vals[i][5]));
      q.setMagnitude(Float.parseFloat(vals[i][6]));
      q.setDepth(Float.parseFloat(vals[i][7]));
      q.setNst(Integer.parseInt(vals[i][8]));
      q.setRegion(vals[i][9]);
      
      this.earthquakes.add(q);
    }
  }

  public List<Earthquake> getLatestForLocation(float lat, float lon, int num) {
    final Predicate<Earthquake> p = new DistanceWithinPredicate(lat, lon, 100);
    
    final List<Earthquake> f = new ArrayList<Earthquake>(Collections2.filter(this.earthquakes, p));
    
    Collections.sort(f, new EQDateComparator());
    
    return new ArrayList<Earthquake>(f.subList(0, Math.max(num, f.size())));
  }
}
