package com.loquake.web.server.dao;

import java.util.ArrayList;
import java.util.List;

import com.floodfx.aboutthattime.AboutThatTime;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.loquake.web.client.dao.EarthquakeService;
import com.loquake.web.client.model.EQResult;
import com.loquake.web.client.model.Earthquake;
import com.loquake.web.server.dao.distance.Haversine;

public class EarthquakeServiceImpl extends RemoteServiceServlet implements EarthquakeService {
  private EarthquakeDAO dao;
  private final EQDateFormat timeFormat = new EQDateFormat();
  
  public EarthquakeServiceImpl() throws Exception {
    this.dao = new FileEarthquakeDAO("/mnt/earthquake/data/eqs7day-M1.txt");
  }

  public List<EQResult> getLatestForLocation(float lat, float lon, int num) {
    final List<EQResult> result = new ArrayList<EQResult>(num);
    
    for (final Earthquake e : dao.getLatestForLocation(lat, lon, num)) {
      final EQResult r = new EQResult();

      r.setQuake(e);
      r.setDistance(Math.round((float)Haversine.distance(lat, lon, e.getLat(), e.getLon())));
      r.setWhen(timeFormat.format(e.getDate()));
      
      result.add(r);
    }
    
    return result;
  }
}
