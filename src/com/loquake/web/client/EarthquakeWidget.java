package com.loquake.web.client;

import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.VisualizationUtils;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.visualizations.LineChart;
import com.google.gwt.visualization.client.visualizations.MapVisualization;
import com.google.gwt.visualization.client.visualizations.LineChart.Options;
import com.loquake.web.client.model.EQResult;
import com.loquake.web.client.model.Earthquake;

public class EarthquakeWidget extends Composite {
  
  private final VerticalPanel panel = new VerticalPanel();
  private final MapDialog map = new MapDialog();
  
  public EarthquakeWidget() {
    panel.setSpacing(4);
    initWidget(panel);
  }
  
  public void setEarthquakes(final List<EQResult> quakes) {
    panel.clear();
    
   panel.add(createGrid(quakes));

    final Runnable callback = new Runnable() {
      public void run() {
        final Options options = Options.create();
        options.setBackgroundColor("white");
        options.setAxisColor("white");
        options.setBorderColor("white");
        options.setShowCategories(false);
        options.setHeight(75);
        options.setWidth(700);
        panel.add(new LineChart(createTable(quakes), options));    
      }
    };

    VisualizationUtils.loadVisualizationApi(callback, LineChart.PACKAGE, MapVisualization.PACKAGE);

  }
  
  private Grid createGrid(final List<EQResult> quakes) {
    final Grid table = new Grid(10, 6);
    
    final DateTimeFormat dtf = DateTimeFormat.getFormat("hh:mm a");
    final NumberFormat nf = NumberFormat.getFormat("#.#");
    
    for (int i=0; i< 10; i++) {
      final EQResult r = quakes.get(i);
      final Earthquake e = r.getQuake();
      
      table.setHTML(i, 0,"<span class='mag'>" + nf.format(e.getMagnitude()) + "</span>");
      table.setHTML(i, 1, "<span class='when'>" + r.getWhen() + "</span> at <span class='hour'>" + dtf.format(e.getDate()) + "</span>");
      table.setHTML(i, 2, "<span class='distance'>" + r.getDistance() + "</span> miles away");
      table.setHTML(i, 3, e.getRegion());
      
      final Anchor m = new Anchor("map");
      m.addClickHandler(new ClickHandler() {
        public void onClick(ClickEvent event) {
          map.showMap(r);
        }
      });
      
      table.setWidget(i, 4, m);

      final Anchor a = new Anchor();
      a.setText("report");
      a.setHref("http://earthquake.usgs.gov/eqcenter/dyfi/events/" + e.getSrc() + "/" + e.getEqid() + "/us/form.en.enabled.html");
      a.setTarget("_blank");

      table.setWidget(i, 5, a);
    }
    
    table.setStylePrimaryName("eq_data");
    
    return table;
  }
  

  
  private DataTable createMapTable(final List<EQResult> quakes) {
    final DataTable table = DataTable.create();
    table.addColumn(ColumnType.NUMBER, "lat");
    table.addColumn(ColumnType.NUMBER, "lon");
    table.addColumn(ColumnType.STRING, "name");
    
    table.addRows(10);
    
    for (int i=0; i< 10; i++) {
      final EQResult r = quakes.get(i);
      final Earthquake e = r.getQuake();
      
      table.setValue(i, 0, e.getLat());
      table.setValue(i, 1, e.getLon());
      table.setValue(i, 2, e.getMagnitude() + " " + e.getRegion());
    }
    
    return table;
  }
  
  private DataTable createTable(final List<EQResult> quakes) {
    final DataTable table = DataTable.create();
    table.addColumn(ColumnType.NUMBER, "magnitude");
    table.addRows(10);
    
    for (int i=0; i< 10; i++) {
      final EQResult r = quakes.get(i);
      final Earthquake e = r.getQuake();
      
      table.setValue(i, 0, e.getMagnitude());
    }
    
    return table;
  }
  
}
