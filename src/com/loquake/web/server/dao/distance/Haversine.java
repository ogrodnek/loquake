package com.loquake.web.server.dao.distance;

/**
 * From: http://groovy.codehaus.org/Compute+distance+from+Google+Earth+Path+%28in+.kml+file%29 
 * @author larry
 *
 */
public class Haversine {
  private static final double R = 3963.1676; // earth radius miles
  public static double distance(final float lat1, final float lon1, final float lat2, final float lon2) {
    double dLat = Math.toRadians(lat2-lat1);
    double dLon = Math.toRadians(lon2-lon1);
    
    double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos( Math.toRadians(lat1) ) *
            Math.cos( Math.toRadians(lat2) ) * Math.sin(dLon/2) * Math.sin(dLon/2);
    
    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    
    return R * c;
  }
}
