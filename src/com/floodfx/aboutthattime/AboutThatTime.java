package com.floodfx.aboutthattime;

import java.util.Date;

/**
 * Gives reasonable approximation of distance of time in words between two
 * dates. Inspired by the ruby and python date helpers...
 * 
 * @author floodfx <donnie@floodfx.com>
 */
public class AboutThatTime {

  private static final int ONE_HOUR = 60;
  private static final int TWO_HOURS = 120;
  private static final int ONE_DAY = 1440;
  private static final int TWO_DAYS = ONE_DAY * 2;
  private static final int ONE_MONTH = 43220;
  private static final int TWO_MONTHS = ONE_MONTH * 2;
  private static final int ONE_YEAR = 525600;
  private static final int TWO_YEARS = ONE_YEAR * 2;

  /**
   * Returns human readable text approximating distance between now and given
   * date. If less than one minute than will default to one minute.
   * 
   * @param from
   *          the date from which to calculate time distance to now
   * @return the human readable text
   */
  public String distanceInWords(final Date from) {
    return distanceInWords(from, false, new Date());
  }

  /**
   * Returns human readable text approximating distance between now and given
   * date. If less than one minute than will approximate seconds if you ask
   * nicely.
   * 
   * @param from
   *          the date from which to calculate time distance to now
   * @param approximateSeconds
   *          show approximate seconds if less than a minute
   * @return the human readable text
   */
  public String distanceInWords(final Date from, final boolean approximateSeconds) {
    return distanceInWords(from, approximateSeconds, new Date());
  }

  /**
   * Returns human readable text approximating distance between given dates If
   * less than one minute than will approximate seconds if you ask nicely.
   * 
   * @param from
   *          the date from which to calculate time distance
   * @param to
   *          the date from which to calculate time distance
   * @param approximateSeconds
   *          show approximate seconds if less than a minute
   * @return the human readable text
   */
  public String distanceInWords(final Date from, final boolean approximateSeconds, final Date to) {
    if (from == null) {
      throw new NullPointerException("'from' date must not be null");
    }

    // default to now
    final Date toOrNow = to != null ? to : new Date();

    // calculated distances
    final long distanceInMillis = Math.abs(toOrNow.getTime() - from.getTime());
    final int distanceInSeconds = (int) (distanceInMillis % 60000) / 1000;
    final long distanceInMinutes = distanceInMillis / 60000;

    if (distanceInMinutes <= 1) {
      if (approximateSeconds && distanceInMinutes != 1) {
        if (distanceInSeconds < 5) {
          return "less than 5 seconds";
        } else if (distanceInSeconds < 10) {
          return "less than 10 seconds";
        } else if (distanceInSeconds < 20) {
          return "less than 20 seconds";
        } else if (distanceInSeconds < 40) {
          return "half a minute";
        } else if (distanceInSeconds < 60) {
          return "less than a minute";
        } else {
          return "1 minute";
        }
      } else {
        return "1 minute";
      }
    } else if (distanceInMinutes < ONE_HOUR) {
      return String.format("%s minutes", String.valueOf(distanceInMinutes));
    } else if (distanceInMinutes < TWO_HOURS) {
      return "1 hour";
    } else if (distanceInMinutes < ONE_DAY) {
      return String.format("%s hours", String.valueOf(distanceInMinutes / ONE_HOUR));
    } else if (distanceInMinutes < TWO_DAYS) {
      return "1 day";
    } else if (distanceInMinutes < ONE_MONTH) {
      return String.format("%s days", String.valueOf(distanceInMinutes / ONE_DAY));
    } else if (distanceInMinutes < TWO_MONTHS) {
      return "1 month";
    } else if (distanceInMinutes < ONE_YEAR) {
      return String.format("%s months", String.valueOf(distanceInMinutes / ONE_MONTH));
    } else if (distanceInMinutes < TWO_YEARS) {
      return "1 year";
    } else {
      return String.format("%s years", String.valueOf(distanceInMinutes / ONE_YEAR));
    }
  }

}