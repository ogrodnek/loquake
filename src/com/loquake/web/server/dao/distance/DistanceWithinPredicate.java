package com.loquake.web.server.dao.distance;

import com.google.common.base.Predicate;
import com.loquake.web.client.model.Earthquake;

public class DistanceWithinPredicate implements Predicate<Earthquake>{
  private final float lat;
  private final float lon;
  private final float distance;
  
  public DistanceWithinPredicate(final float lat, final float lon, final float distance) {
    this.lat = lat;
    this.lon = lon;
    this.distance = distance;
  }
  
  public boolean apply(final Earthquake q) {
    double d = Haversine.distance(lat, lon, q.getLat(), q.getLon());
    
    return d < distance;
  }
}
