package com.loquake.web.client.dao;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.loquake.web.client.model.EQResult;

public interface EarthquakeServiceAsync {
  void getLatestForLocation(float lat, float lon, int num, AsyncCallback<List<EQResult>> callback);
}
