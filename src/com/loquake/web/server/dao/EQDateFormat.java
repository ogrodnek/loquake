package com.loquake.web.server.dao;

import java.util.Date;

import com.floodfx.aboutthattime.AboutThatTime;

public class EQDateFormat {
  private final AboutThatTime timeFormat = new AboutThatTime();
    public String format(final Date d) {
      final String ret = timeFormat.distanceInWords(d);
      
      if ("1 day".equals(ret)) {
        return "yesterday";
      }
      
    return ret + " ago";
  }
}
